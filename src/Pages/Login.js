import {useState, useEffect} from 'react';


import { Form, Button } from 'react-bootstrap';

export default function Register() {

    const [username, setUsername] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

/*
if(!email && !password1 && !password2 && password1 === password2)
if ( email !== null && password1 !== null && password2 !== null && password1 === password2)
*/


    useEffect(() => {
        if(( username !== "" && password1 !== "" && password2 !== "") && password1 === password2) {
            setIsActive(true);
        } else {
            setIsActive(false);
        };
    }, [username, password1, password2]);


    function registerUser(e) {
        e.preventDefault();

        setUsername("");
        setPassword1("");
        setPassword2("");

        alert("Thank you for registering!");
    };

    return (
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userLogin">
                <Form.Label>Username</Form.Label>
                <Form.Control 
	                type="username" 
	                placeholder="Enter Username" 
	                value={username}
                    onChange={e => setUsername(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>


            }


        </Form>
    )

}

