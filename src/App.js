import {useState} from 'react';
import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css';

function App() {

  const [user, setUser] = useState({email: localStorage.getItem('email')});

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    // <></> fragments - common pattern in React for component to return multiple elements
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        < AppNavbar/>
        <Container>
        <Routes>
          < Route path="/" element={<Home/>}/>
          < Route path="/courses" element={<Courses/>}/>
          < Route path="/register" element={<Register/>}/>
          < Route path="/login" element={<Login />}/>
          < Route path="/logout" element={<Logout/>}/>
          < Route path="/*" element={<Error />}/>
        </Routes>
        </Container>
      </Router>
      </UserProvider>
    </>
  );
}

export default App;




